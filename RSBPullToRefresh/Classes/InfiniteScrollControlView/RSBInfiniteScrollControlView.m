//
//  KAInfiniteScrollControlView.m
//  Collaborate
//
//  Created by Anton on 24.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "RSBInfiniteScrollControlView.h"

@implementation RSBInfiniteScrollControlView

- (id) initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        self.loaderView = [[RSBLoaderView alloc] init];
        [self addSubview:self.loaderView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    self.loaderView.frame = CGRectInset(self.bounds, 16.0, 16.0);
}

@end
