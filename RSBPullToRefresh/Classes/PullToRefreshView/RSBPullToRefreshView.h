//
//  KAPullToRefreshView.h
//  Collaborate
//
//  Created by Anton on 24.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSBLoaderView.h"
#import "RSBProgressView.h"

@interface RSBPullToRefreshView : UIView

@property (nonatomic, strong) RSBProgressView *progressView;
@property (nonatomic, strong) RSBLoaderView *loaderView;

@end
