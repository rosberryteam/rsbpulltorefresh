//
//  KAPullToRefreshView.m
//  Collaborate
//
//  Created by Anton on 24.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "RSBPullToRefreshView.h"

@implementation RSBPullToRefreshView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.clipsToBounds = YES;
        
        self.loaderView = [[RSBLoaderView alloc] init];
        self.loaderView.hidden = YES;
        [self addSubview:self.loaderView];
        
        self.progressView = [[RSBProgressView alloc] init];
        [self addSubview:self.progressView];
    }
    return self;
}

- (void) setHidden:(BOOL)hidden {
    [super setHidden:hidden];
}

- (void) layoutSubviews {
    [super layoutSubviews];

    self.loaderView.frame = (CGRect) {
        .origin.y = 16.0,
        .size.width = self.bounds.size.width,
        .size.height = 28.0,
    };
    
    self.progressView.frame = self.loaderView.frame;
    
    if(!self.loaderView.isAnimating) {
        [self.loaderView startAnimating];
    }
}

@end
