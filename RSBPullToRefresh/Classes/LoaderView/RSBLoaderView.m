//
//  CircleLoaderView.m
//  Collaborate
//
//  Created by Anton on 28.05.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "RSBLoaderView.h"

@interface RSBLoaderView ()

@property (nonatomic, strong) CAShapeLayer *circleLayer;
@property (nonatomic, strong) CAShapeLayer *backCircleLayer;
@property (nonatomic, strong) UIView *mainSubview;

@property (nonatomic, strong) CADisplayLink *animationTimer;
@property (nonatomic, assign) CGFloat rotation;

@end

@implementation RSBLoaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _lineWidth = 1.f;
        _circleColor = [UIColor clearColor];
        _fillColor = [UIColor blackColor];
        _animationDuration = 1.f;
        
        _mainSubview = [[UIView alloc] init];
        [_mainSubview setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_mainSubview];
        
        _animationTimer = [CADisplayLink displayLinkWithTarget:self selector:@selector(onAnimationTimer:)];
        _animationTimer.paused = YES;
        [_animationTimer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    }
    return self;
}

- (void)dealloc {
    [self.animationTimer invalidate];
    self.animationTimer = nil;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [_mainSubview setBounds:self.bounds];
    [_mainSubview setCenter:(CGPoint){
        .x = self.bounds.size.width / 2.0,
        .y = self.bounds.size.height / 2.0,
    }];
    
    double side = MIN(CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
    double radius = side / 2.0f;
    double lineHalfWidth = _lineWidth / 2.f;
    
    CGPoint center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f, CGRectGetHeight(self.bounds) / 2.f);
    CGRect pathRect = (CGRect) {
        .origin.x = CGRectGetWidth(self.frame) / 2.f - radius + lineHalfWidth,
        .origin.y = CGRectGetHeight(self.frame) / 2.f - radius + lineHalfWidth,
        .size.width = side - self.lineWidth,
        .size.height = side - self.lineWidth,
    };
    
    UIBezierPath *fullCirclePath = [UIBezierPath bezierPathWithOvalInRect:pathRect];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithArcCenter:center radius:(radius - lineHalfWidth) startAngle:-M_PI_2 endAngle:(M_PI + M_PI_4) clockwise:YES];
    
    if (!_circleLayer) {
        _backCircleLayer = [CAShapeLayer layer];
        [_backCircleLayer setStrokeColor:_circleColor.CGColor];
        [_backCircleLayer setFillColor:[UIColor clearColor].CGColor];
        [_backCircleLayer setLineWidth:_lineWidth];
        [self.layer addSublayer:_backCircleLayer];
        
        _circleLayer = [CAShapeLayer layer];
        [_circleLayer setLineCap:kCALineCapRound];
        [_circleLayer setStrokeColor:_fillColor.CGColor];
        [_circleLayer setFillColor:[UIColor clearColor].CGColor];
        [_circleLayer setLineWidth:_lineWidth];
        [_mainSubview.layer addSublayer:_circleLayer];
    }
    
    [_backCircleLayer setPath:fullCirclePath.CGPath];
    [_circleLayer setPath:circlePath.CGPath];
    
    _circleLayer.frame = _mainSubview.bounds;
    _backCircleLayer.frame = _mainSubview.bounds;
}

- (void)setLineWidth:(double)lineWidth {
    _lineWidth = lineWidth;
    [self setNeedsLayout];
}

- (void)sizeToFit {
    self.bounds = (CGRect){
        .size.width = 24.0,
        .size.height = 24.0,
    };
}

- (void) setHidden:(BOOL)hidden {
    [super setHidden:hidden];
    
    if (hidden) {
        [self stopAnimating];
    }
}

- (void)startAnimating {
    self.animationTimer.paused = NO;
}

- (void)stopAnimating {
    self.animationTimer.paused = YES;
    self.rotation = 0.0;
}

- (BOOL)isAnimating {
    return !self.animationTimer.paused;
}

- (void)onAnimationTimer:(CADisplayLink *)sender {
    self.rotation += (sender.duration * M_PI * 2.0) / (self.animationDuration);
}

- (void)setRotation:(CGFloat)rotation {
    while (rotation >= (M_PI * 2.0)) {
        rotation -= (M_PI * 2.0);
    }
    
    _rotation = rotation;
    _mainSubview.transform = CGAffineTransformMakeRotation(rotation);
}

@end
