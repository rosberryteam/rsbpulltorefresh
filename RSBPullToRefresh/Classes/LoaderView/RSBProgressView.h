//
//  CircleProgressView.h
//  Collaborate
//
//  Created by Anton on 27.05.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSBProgressView : UIView

@property (nonatomic, strong) UIColor *circleColor;
@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, assign) double progress;
@property (nonatomic, assign) double lineWidth;

- (void)setProgress:(double)progress animated:(BOOL)animated;

@end
