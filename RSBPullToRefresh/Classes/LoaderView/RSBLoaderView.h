//
//  CircleLoaderView.h
//  Collaborate
//
//  Created by Anton on 28.05.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSBLoaderView : UIView

@property (nonatomic, assign) double lineWidth;
@property (nonatomic, assign) double animationDuration;
@property (nonatomic, strong) UIColor *circleColor;
@property (nonatomic, strong) UIColor *fillColor;

- (void)startAnimating;
- (BOOL)isAnimating;
- (void)stopAnimating;

@end
