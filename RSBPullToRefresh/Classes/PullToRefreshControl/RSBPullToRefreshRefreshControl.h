//
//  KARefreshControl.h
//  Collaborate
//
//  Created by Anton on 23.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSBRefreshControlProtocol.h"

@interface RSBPullToRefreshRefreshControl : NSObject <RSBRefreshControlProtocol>

@end
