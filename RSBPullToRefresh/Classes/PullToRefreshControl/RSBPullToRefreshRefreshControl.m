//
//  KARefreshControl.m
//  Collaborate
//
//  Created by Anton on 23.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "RSBPullToRefreshRefreshControl.h"
#import "RSBPullToRefreshView.h"

@implementation RSBPullToRefreshRefreshControl

@synthesize view, refreshControlHeight, refreshControlActivationInset, refreshControlInset, isLoading, actionHandlerBlock, enabled, delegate;

- (instancetype)init {
    self = [super init];
    if (self) {
        RSBPullToRefreshView *refreshView = [[RSBPullToRefreshView alloc] init];
        refreshView.hidden = YES;
        
        self.view = refreshView;
        self.refreshControlHeight = 60.0;
        self.refreshControlActivationInset = 60.0;
        self.enabled = YES;
    }
    return self;
}

- (void)refreshControlShowWithProgress:(double)progress {
    RSBPullToRefreshView *refreshView = (RSBPullToRefreshView *)self.view;
    refreshView.progressView.progress = progress * 0.9;
    refreshView.progressView.hidden = (progress >= 1.0) || self.isLoading;
    refreshView.loaderView.hidden = !refreshView.progressView.hidden;
}

- (void)startLoading {
    self.isLoading = YES;
    
    RSBPullToRefreshView *refreshView = (RSBPullToRefreshView *)self.view;
    refreshView.hidden = NO;
    [refreshView.loaderView startAnimating];
    refreshView.loaderView.hidden = NO;
    refreshView.progressView.hidden = YES;
}

- (void)endLoading {

    [self.delegate refreshControlDidStopAnimating:self animated:YES];
}

- (void)endLoadingNow {
    
    [self.delegate refreshControlDidStopAnimating:self animated:NO];
}

- (void) setEnabled:(BOOL)_enabled {
    enabled = _enabled;
    self.view.hidden = !enabled;
}

@end
