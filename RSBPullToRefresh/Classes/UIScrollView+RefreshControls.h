//
//  UIScrollView+RefreshControls.h
//  Collaborate
//
//  Created by Anton on 24.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSBRefreshControlProtocol.h"

#ifdef RSB_RC_WITH_REACTIVE
@class RACSignal;
#endif

typedef enum {
    RSBPullToRefreshStyleUnderContent,
    RSBPullToRefreshStyleMovesWithContent,
} RSBPullToRefreshStyle;

@interface UIScrollView (RefreshControls)

@property (nonatomic, strong, readonly) id<RSBRefreshControlProtocol> pullToRefreshControl;
@property (nonatomic, strong, readonly) id<RSBRefreshControlProtocol> infiniteScrollingRefreshControl;
@property (nonatomic, strong, readonly) id<RSBRefreshControlProtocol> reverseInfiniteScrollingRefreshControl;

@property (nonatomic, strong) NSNumber *initialBottomInsets;
@property (nonatomic, strong) NSNumber *initialTopInsets;

@property (nonatomic, assign) RSBPullToRefreshStyle pullToRefreshStyle;

- (id<RSBRefreshControlProtocol>)addPullToRefreshWithActionHandler:(RSBActionHandlerBlock)handler;

- (id<RSBRefreshControlProtocol>)addInfiniteScrollingWithActionHandler:(RSBActionHandlerBlock)handler;
- (id<RSBRefreshControlProtocol>)addReverseInfiniteScrollingWithActionHandler:(RSBActionHandlerBlock)handler;

- (void)addInfiniteScrollingWithRefreshControl:(id<RSBRefreshControlProtocol>)refreshControl actionHandler:(RSBActionHandlerBlock)handler;
- (void)addPullToRefreshWithRefreshControl:(id<RSBRefreshControlProtocol>)refreshControl actionHandler:(RSBActionHandlerBlock)handler;

- (void)triggerPullToRefresh;

#ifdef RSB_RC_WITH_REACTIVE
- (RACSignal *)rac_addInfiniteScrollingSignal;
- (RACSignal *)rac_addPullToRefreshSignal;
- (RACSignal *)rac_addReverseInfiniteScrollingSignal;
- (RACSignal *)rac_addInfiniteScrollingWithRefreshControlSignal:(id<RSBRefreshControlProtocol>)refreshControl;
- (RACSignal *)rac_addPullToRefreshWithRefreshControlSignal:(id<RSBRefreshControlProtocol>)refreshControl;
#endif

@end
