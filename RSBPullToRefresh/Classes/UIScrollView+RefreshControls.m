//
//  UIScrollView+RefreshControls.m
//  Collaborate
//
//  Created by Anton on 24.06.15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "UIScrollView+RefreshControls.h"

#import "RSBPullToRefreshRefreshControl.h"
#import "RSBInfiniteScrollRefreshControl.h"

#import <objc/runtime.h>

#ifdef RSB_RC_WITH_REACTIVE
#import <ReactiveCocoa/ReactiveCocoa.h>
#endif

static NSString * const KARefreshControlContentInsetKey = @"contentInset";
static NSString * const KARefreshControlContentOffsetKey = @"contentOffset";
static NSString * const KARefreshControlContentSizeKey = @"contentSize";
static NSString * const KARefreshControlPanStateKey = @"pan.state";

@interface UIScrollView (PrivateScrollView) <RSBRefreshControlDelegate>

@property (nonatomic, strong) UIView *contentSizeView;
@property (nonatomic, strong) NSLayoutConstraint *contentSizeViewHeigthConstraint;

@property (nonatomic, assign) BOOL refreshControlShouldStop;
@property (nonatomic, assign) BOOL infiniteControlShouldStop;
@property (nonatomic, assign) BOOL observing;

@property (nonatomic, assign) CGSize lastContentSize;

@property (nonatomic) BOOL infiniteScrollControlHeightInstalled;
@property (nonatomic) BOOL reverseInfiniteScrollControlHeightInstalled;

@end

@implementation UIScrollView (RefreshControls)

#pragma mark - Swizzling dealloc

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        SEL originalSelector = NSSelectorFromString(@"dealloc");
        SEL swizzledSelector = @selector(ka_rc_swizzled_dealloc);
        
        Method originalMethod = class_getInstanceMethod(class, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
        
        BOOL didAddMethod = class_addMethod(class, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
        
        if (didAddMethod) {
            class_replaceMethod(class, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
    });
}

- (void)ka_rc_swizzled_dealloc {
    if (self.observing) {
        [self removeObserver:self forKeyPath:KARefreshControlContentOffsetKey];
        [self removeObserver:self forKeyPath:KARefreshControlContentSizeKey];
        [self removeObserver:self forKeyPath:KARefreshControlPanStateKey];
        [self removeObserver:self forKeyPath:KARefreshControlContentInsetKey];
        self.observing = NO;
    }
    [self ka_rc_swizzled_dealloc]; //calls original dealloc
}

#pragma mark - Observing

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:KARefreshControlContentOffsetKey]) {
        [self scrollViewDidScroll:[[change valueForKey:NSKeyValueChangeNewKey] CGPointValue]];
    } else if([keyPath isEqualToString:KARefreshControlContentSizeKey]) {
        [self.contentSizeViewHeigthConstraint setConstant:self.contentSize.height];
        [self.contentSizeView layoutIfNeeded];
    } else if ([keyPath isEqualToString:KARefreshControlPanStateKey]) {
        UIGestureRecognizerState state = [[change valueForKey:NSKeyValueChangeNewKey] integerValue];
        if (self.refreshControlShouldStop && state == UIGestureRecognizerStateEnded) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.pullToRefreshControl endLoading];
                self.refreshControlShouldStop = NO;
            });
        }
        
        if (self.infiniteControlShouldStop && state == UIGestureRecognizerStateEnded) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.infiniteScrollingRefreshControl endLoading];
                self.infiniteControlShouldStop = NO;
            });
        }
    }
    else if ([keyPath isEqualToString:KARefreshControlContentInsetKey]) {
        if (!self.infiniteScrollControlHeightInstalled) {
            self.initialBottomInsets = @(self.contentInset.bottom);
        }
        if (!self.reverseInfiniteScrollControlHeightInstalled) {
            self.initialTopInsets = @(self.contentInset.top);
        }
    }
}

- (void)updateRefreshControlPositionWithOffset:(CGFloat)offset {
    [self.pullToRefreshControl refreshControlShowWithProgress:offset];
    
    switch (self.pullToRefreshStyle) {
        case RSBPullToRefreshStyleUnderContent: {
            [self updateRefreshControlPositionUnderContent:offset];
            break;
        }
        case RSBPullToRefreshStyleMovesWithContent: {
            [self updateRefreshControlMovesWithContent:offset];
            break;
        }
    }
    [self.pullToRefreshControl.view layoutIfNeeded];
}

- (void) updateRefreshControlPositionUnderContent:(CGFloat)offset {
    if(self.pullToRefreshControl.isLoading) {
        if (!self.contentSize.height) {
            offset = 1.0;
        }
        self.pullToRefreshControl.view.frame = (CGRect) {
            .origin.y = self.contentOffset.y + self.pullToRefreshControl.refreshControlInset + [self.initialTopInsets doubleValue],
            .size.width = self.bounds.size.width,
            .size.height = MAX(MIN(offset, 1.0), 0.0) * self.pullToRefreshControl.refreshControlHeight,
        };
    } else {
        self.pullToRefreshControl.view.frame = (CGRect) {
            .origin.y = self.contentOffset.y + self.pullToRefreshControl.refreshControlInset + self.contentInset.top,
            .size.width = self.bounds.size.width,
            .size.height = MAX(offset, 0.0) * self.pullToRefreshControl.refreshControlHeight,
        };
    }
}

- (void) updateRefreshControlMovesWithContent:(CGFloat)offset {
    self.pullToRefreshControl.view.frame = (CGRect) {
        .origin.y = self.contentSize.height ? -self.pullToRefreshControl.refreshControlHeight : 0.f,
        .size.width = self.bounds.size.width,
        .size.height = self.pullToRefreshControl.refreshControlHeight,
    };
}

- (void)updateInfiniteControlPositionWithOffset:(CGFloat)offset {
    self.infiniteScrollingRefreshControl.view.hidden = !self.infiniteScrollingRefreshControl.isLoading;
    self.infiniteScrollingRefreshControl.view.frame = (CGRect) {
        .origin.y = self.contentSize.height - self.infiniteScrollingRefreshControl.refreshControlInset,
        .size.width = self.bounds.size.width,
        .size.height = self.infiniteScrollingRefreshControl.refreshControlHeight,
    };
    [self.infiniteScrollingRefreshControl.view layoutIfNeeded];
}

- (void)updateReverseInfiniteControlPositionWithOffset:(CGFloat)offset {
    self.reverseInfiniteScrollingRefreshControl.view.hidden = !self.reverseInfiniteScrollingRefreshControl.isLoading;
    self.reverseInfiniteScrollingRefreshControl.view.frame = (CGRect) {
        .origin.y = self.reverseInfiniteScrollingRefreshControl.refreshControlInset - self.reverseInfiniteScrollingRefreshControl.refreshControlHeight,
        .size.width = self.bounds.size.width,
        .size.height = self.reverseInfiniteScrollingRefreshControl.refreshControlHeight,
    };
    [self.reverseInfiniteScrollingRefreshControl.view layoutIfNeeded];
}

- (void)startInifiniteControlLoading {
    [self.infiniteScrollingRefreshControl refreshControlShowWithProgress:0.0];
    [self.infiniteScrollingRefreshControl startLoading];
    if (self.infiniteScrollingRefreshControl.actionHandlerBlock)
        self.infiniteScrollingRefreshControl.actionHandlerBlock();
}

- (void)stopInfiniteControlLoading {
    [self.infiniteScrollingRefreshControl endLoadingNow];
    [self updateInfiniteControlPositionWithOffset:0.0];
}

- (void)startReverseInfiniteControlLoading {
    [self.reverseInfiniteScrollingRefreshControl refreshControlShowWithProgress:0.0];
    [self.reverseInfiniteScrollingRefreshControl startLoading];
    if (self.reverseInfiniteScrollingRefreshControl.actionHandlerBlock)
        self.reverseInfiniteScrollingRefreshControl.actionHandlerBlock();
}

- (void)stopReverseInfiniteControlLoading {
    [self.reverseInfiniteScrollingRefreshControl endLoadingNow];
    [self updateReverseInfiniteControlPositionWithOffset:0.0];
}

- (void)startPullToRefreshControlLoading {
    [self.pullToRefreshControl startLoading];
    
    self.initialTopInsets = @(self.contentInset.top);
    
    [self installTopInset];
    
    if (self.pullToRefreshControl.refreshControlHeight) {
        CGPoint currentTranslation = [self.panGestureRecognizer translationInView:self];
        currentTranslation.y -= [self.pullToRefreshControl refreshControlHeight];
        [self.panGestureRecognizer setTranslation:currentTranslation inView:self];
    }
    
    if (self.pullToRefreshControl.actionHandlerBlock)
        self.pullToRefreshControl.actionHandlerBlock();
}

- (void)scrollViewDidScroll:(CGPoint)contentOffset {
    
    if (self.pullToRefreshControl) {
        UIEdgeInsets insets = self.contentInset;
        if (((contentOffset.y + insets.top <= 0.f) || self.pullToRefreshControl.isLoading) && self.pullToRefreshControl.enabled) {

            double offset = -(contentOffset.y + (self.pullToRefreshControl.isLoading ? [self.initialTopInsets doubleValue] : insets.top));
            double height = self.pullToRefreshControl.refreshControlActivationInset;
            double progress = MAX(offset/height, 0.0);
            
            if (progress >= 1) {
                if (!self.pullToRefreshControl.isLoading && !self.refreshControlShouldStop && [self.panGestureRecognizer velocityInView:self].y > 0) {
                    [self startPullToRefreshControlLoading];
                }
            }
            [self updateRefreshControlPositionWithOffset:progress];
        }
    }
    
    if (!self.contentSize.height)
        return;

    if (self.infiniteScrollingRefreshControl) {
        if ((contentOffset.y >= (self.contentSize.height - CGRectGetHeight(self.bounds)))) {
            if (!self.infiniteScrollingRefreshControl.isLoading && self.infiniteScrollingRefreshControl.enabled) {
                [self installBottomInset];
                if(!self.infiniteScrollingRefreshControl.isLoading)
                    [self startInifiniteControlLoading];
                
            } else if (!self.infiniteScrollingRefreshControl.enabled) {
                [self removeBottomInset];
            }
        } else if (self.contentSize.height <= CGRectGetHeight(self.bounds)) {
            [self removeBottomInset];
        }
        
        [self updateInfiniteControlPositionWithOffset:0.0];
    }
    
    if (self.reverseInfiniteScrollingRefreshControl) {
        if ((self.isDecelerating || self.isDragging) && (contentOffset.y < -self.contentInset.top)) {
            if (!self.reverseInfiniteScrollingRefreshControl.isLoading && self.reverseInfiniteScrollingRefreshControl.enabled) {
                [self installTopInset];
                if (!self.reverseInfiniteScrollingRefreshControl.isLoading)
                    [self startReverseInfiniteControlLoading];
                
                self.lastContentSize = self.contentSize;
            } else if (!self.reverseInfiniteScrollingRefreshControl.enabled) {
                [self removeTopInset];
                [self.reverseInfiniteScrollingRefreshControl.view setHidden:YES];
            }
        } else if (self.contentSize.height <= CGRectGetHeight(self.bounds)) {
            [self removeTopInset];
        }
    
        [self updateReverseInfiniteControlPositionWithOffset:0.0];
    }
}

#pragma mark - Methods

- (void)triggerPullToRefresh {
    
    if (self.pullToRefreshControl.isLoading) {
        [self.pullToRefreshControl startLoading];
        return;
    }
    
    if (self.pullToRefreshControl) {
        if (!self.contentSize.height && self.pullToRefreshControl.refreshControlHeight) {
            [self.pullToRefreshControl startLoading];
            self.initialTopInsets = @(self.contentInset.top);
            if (self.pullToRefreshControl.actionHandlerBlock)
                self.pullToRefreshControl.actionHandlerBlock();
            [self updateRefreshControlPositionWithOffset:1.0];
        } else {
            [self layoutIfNeeded];
            self.initialTopInsets = @(self.contentInset.top);
            self.pullToRefreshControl.isLoading = YES;
            if (self.pullToRefreshControl.actionHandlerBlock) {
                self.pullToRefreshControl.actionHandlerBlock();
            }
        }
    }
}

- (id<RSBRefreshControlProtocol>)addPullToRefreshWithActionHandler:(RSBActionHandlerBlock)handler {
    RSBPullToRefreshRefreshControl *refreshControl = [[RSBPullToRefreshRefreshControl alloc] init];
    [self addPullToRefreshWithRefreshControl:refreshControl actionHandler:handler];
    
    return refreshControl;
}

- (id<RSBRefreshControlProtocol>)addInfiniteScrollingWithActionHandler:(RSBActionHandlerBlock)handler {
    RSBInfiniteScrollRefreshControl *refreshControl = [[RSBInfiniteScrollRefreshControl alloc] init];
    [self addInfiniteScrollingWithRefreshControl:refreshControl actionHandler:handler];
    
    return refreshControl;
}

- (void)addPullToRefreshWithRefreshControl:(id<RSBRefreshControlProtocol>)refreshControl actionHandler:(RSBActionHandlerBlock)handler {
    if (self.reverseInfiniteScrollingRefreshControl) {
        [self.reverseInfiniteScrollingRefreshControl.view removeFromSuperview];
        self.reverseInfiniteScrollingRefreshControl = nil;
    }
    
    refreshControl.refreshControlInset = self.contentInset.top;
    [self addRefreshControl:refreshControl withActionHandler:handler];
    self.pullToRefreshControl = refreshControl;
}

- (id<RSBRefreshControlProtocol>)addReverseInfiniteScrollingWithActionHandler:(RSBActionHandlerBlock)handler {
    if (self.pullToRefreshControl) {
        [self.pullToRefreshControl.view removeFromSuperview];
        self.pullToRefreshControl = nil;
    }
    
    RSBInfiniteScrollRefreshControl *refreshControl = [[RSBInfiniteScrollRefreshControl alloc] init];
    self.reverseInfiniteScrollingRefreshControl = refreshControl;
    [self addInfiniteControl:refreshControl withActionHandler:handler];
    [self.reverseInfiniteScrollingRefreshControl setActionHandlerBlock:handler];
    self.reverseInfiniteScrollingRefreshControl.enabled = YES;
    
    return refreshControl;
}

- (void)addInfiniteScrollingWithRefreshControl:(id<RSBRefreshControlProtocol>)refreshControl actionHandler:(RSBActionHandlerBlock)handler {
    self.infiniteScrollingRefreshControl = refreshControl;
    [self addInfiniteControl:refreshControl withActionHandler:handler];
    [self.infiniteScrollingRefreshControl setActionHandlerBlock:handler];
    self.infiniteScrollingRefreshControl.enabled = YES;
}

- (void)addRefreshControl:(id<RSBRefreshControlProtocol>)refreshControl withActionHandler:(RSBActionHandlerBlock)handler {
    
    if (!self.observing) {
        self.observing = YES;
        [self addObserver:self forKeyPath:KARefreshControlContentOffsetKey options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:KARefreshControlContentSizeKey options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:KARefreshControlPanStateKey options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:KARefreshControlContentInsetKey options:NSKeyValueObservingOptionNew context:nil];
    }
    
    [refreshControl setActionHandlerBlock:handler];
    [refreshControl setDelegate:self];
    [refreshControl.view setTranslatesAutoresizingMaskIntoConstraints:NO];

    self.initialTopInsets = @(self.contentInset.top);
    self.initialBottomInsets = @(self.contentInset.bottom);
    
    [self updateRefreshControlPositionWithOffset:0.0];
    [self updateInfiniteControlPositionWithOffset:0.0];

    [self addSubview:refreshControl.view];
    
    if (!self.contentSizeView) {
        self.contentSizeView = [[UIView alloc] init];
        [self.contentSizeView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentSizeView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.contentSizeView];
        
        NSDictionary *views = @{@"view":self.contentSizeView};
        NSDictionary *metrics = @{};
        NSArray *ch = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:metrics views:views];
        self.contentSizeViewHeigthConstraint = [NSLayoutConstraint constraintWithItem:self.contentSizeView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:0];
        NSLayoutConstraint *cy = [NSLayoutConstraint constraintWithItem:self.contentSizeView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        [self.contentSizeView addConstraint:self.contentSizeViewHeigthConstraint];
        [self addConstraints:[ch arrayByAddingObjectsFromArray:@[cy]]];
    }
}

- (void)addInfiniteControl:(id<RSBRefreshControlProtocol>)refreshControl withActionHandler:(RSBActionHandlerBlock)handler {
    [self addRefreshControl:refreshControl withActionHandler:handler];
}

- (void)installTopInset {
    if (!self.reverseInfiniteScrollControlHeightInstalled) {
        self.reverseInfiniteScrollControlHeightInstalled = YES;
        self.initialTopInsets = @(self.contentInset.top);
        
        UIEdgeInsets insets = self.contentInset;
        if (self.reverseInfiniteScrollingRefreshControl) {
            insets.top += self.reverseInfiniteScrollingRefreshControl.refreshControlHeight;
        } else {
            insets.top += self.pullToRefreshControl.refreshControlHeight;
        }
        [self setContentInset:insets];
    }
}

- (void)removeTopInset {
    if (self.reverseInfiniteScrollControlHeightInstalled) {
        self.reverseInfiniteScrollControlHeightInstalled = NO;
        
        UIEdgeInsets insets = self.contentInset;
        insets.top = self.initialTopInsets.doubleValue;
        self.contentInset = insets;
    }
}

- (void)installBottomInset {
    if (!self.infiniteScrollControlHeightInstalled) {
        self.infiniteScrollControlHeightInstalled = YES;
        self.initialBottomInsets = @(self.contentInset.bottom);
        
        UIEdgeInsets insets = self.contentInset;
        insets.bottom += self.infiniteScrollingRefreshControl.refreshControlHeight;
        [self setContentInset:insets];
    }
}

- (void)removeBottomInset {
    if (self.infiniteScrollControlHeightInstalled) {
        self.infiniteScrollControlHeightInstalled = NO;
        
        UIEdgeInsets insets = self.contentInset;
        insets.bottom = self.initialBottomInsets.doubleValue;
        self.contentInset = insets;
    }
}

#pragma mark - KARefreshControlDelegate

- (void)refreshControlDidStopAnimating:(id<RSBRefreshControlProtocol>)control animated:(BOOL)animated {
    if (!control.isLoading) {
        return;
    }
    
    if ([control isEqual:self.pullToRefreshControl]) {
        if (self.tracking) {
            self.refreshControlShouldStop = YES;
            return;
        }
        
        [UIView setAnimationsEnabled:animated];
        [UIView animateWithDuration:0.25 animations:^{
            [self removeTopInset];
        } completion:^(BOOL finished) {
            [self updateRefreshControlPositionWithOffset:0.0];
        }];
        [UIView setAnimationsEnabled:YES];

        self.refreshControlShouldStop = NO;
        self.pullToRefreshControl.isLoading = NO;
        
    } else if ([control isEqual:self.reverseInfiniteScrollingRefreshControl]) {
        
        BOOL isAnimated = !self.reverseInfiniteScrollingRefreshControl.enabled && animated;
        if (isAnimated) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.25];
        }
        
        [self removeTopInset];
        CGSize nContentSize = self.contentSize;
        double offset = nContentSize.height - self.lastContentSize.height - self.reverseInfiniteScrollingRefreshControl.refreshControlHeight * self.reverseInfiniteScrollingRefreshControl.enabled;
        
        if (offset > 0) {
            [self setContentOffset:CGPointMake(0, offset + self.contentOffset.y)];
        } else {
            [self setContentOffset:CGPointMake(0.0, -self.contentInset.top) animated:YES];
        }
        
        if(isAnimated) {
            [UIView commitAnimations];
        }
        
        [self updateReverseInfiniteControlPositionWithOffset:0.0];
        
        self.infiniteControlShouldStop = NO;
        self.reverseInfiniteScrollingRefreshControl.isLoading = NO;
        
    } else if ([control isEqual:self.infiniteScrollingRefreshControl]) {
        
        [UIView setAnimationsEnabled:animated];
        [UIView animateWithDuration:0.25 animations:^{
            [self removeBottomInset];
        } completion:^(BOOL finished) {
            [self updateInfiniteControlPositionWithOffset:0.0];
        }];
        [UIView setAnimationsEnabled:YES];
        
        [self updateInfiniteControlPositionWithOffset:0.0];
        
        self.infiniteControlShouldStop = NO;
        self.infiniteScrollingRefreshControl.isLoading = NO;
    }
}

#pragma mark - Setters

- (void)setPullToRefreshControl:(id<RSBRefreshControlProtocol>)pullToRefreshControl {
    objc_setAssociatedObject(self, @selector(setPullToRefreshControl:), pullToRefreshControl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setInfiniteScrollingRefreshControl:(id<RSBRefreshControlProtocol>)infiniteScrollingRefreshControl{
    objc_setAssociatedObject(self, @selector(setInfiniteScrollingRefreshControl:), infiniteScrollingRefreshControl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setReverseInfiniteScrollingRefreshControl:(id<RSBRefreshControlProtocol>)reverseInfiniteScrollingRefreshControl {
    objc_setAssociatedObject(self, @selector(setReverseInfiniteScrollingRefreshControl:), reverseInfiniteScrollingRefreshControl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setContentSizeView:(UIView *)contentSizeView {
    objc_setAssociatedObject(self, @selector(setContentSizeView:), contentSizeView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setContentSizeViewHeigthConstraint:(NSLayoutConstraint *)contentSizeViewHeigthConstraint {
    objc_setAssociatedObject(self, @selector(setContentSizeViewHeigthConstraint:), contentSizeViewHeigthConstraint, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setRefreshControlShouldStop:(BOOL)refreshControlShouldStop {
    objc_setAssociatedObject(self, @selector(setRefreshControlShouldStop:), @(refreshControlShouldStop), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setInfiniteControlShouldStop:(BOOL)infiniteControlShouldStop {
    objc_setAssociatedObject(self, @selector(setInfiniteControlShouldStop:), @(infiniteControlShouldStop), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setObserving:(BOOL)observing {
    objc_setAssociatedObject(self, @selector(setObserving:), @(observing), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setInitialBottomInsets:(NSNumber *)initialBottomInsets {
    objc_setAssociatedObject(self, @selector(setInitialBottomInsets:), initialBottomInsets, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setInitialTopInsets:(NSNumber *)initialTopInsets {
    objc_setAssociatedObject(self, @selector(setInitialTopInsets:), initialTopInsets, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setLastContentSize:(CGSize)lastContentSize {
    objc_setAssociatedObject(self, @selector(setLastContentSize:), [NSValue valueWithCGSize:lastContentSize], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setInfiniteScrollControlHeightInstalled:(BOOL)infiniteScrollControlHeightInstalled {
    objc_setAssociatedObject(self, @selector(setInfiniteScrollControlHeightInstalled:), @(infiniteScrollControlHeightInstalled), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setReverseInfiniteScrollControlHeightInstalled:(BOOL)reverseInfiniteScrollControlHeightInstalled {
    objc_setAssociatedObject(self, @selector(setReverseInfiniteScrollControlHeightInstalled:), @(reverseInfiniteScrollControlHeightInstalled), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setPullToRefreshStyle:(RSBPullToRefreshStyle)pullToRefreshStyle {
    objc_setAssociatedObject(self, @selector(setPullToRefreshStyle:), @(pullToRefreshStyle), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Getters

- (id<RSBRefreshControlProtocol>)pullToRefreshControl {
    return objc_getAssociatedObject(self, @selector(setPullToRefreshControl:));
}

- (id<RSBRefreshControlProtocol>)infiniteScrollingRefreshControl {
    return objc_getAssociatedObject(self, @selector(setInfiniteScrollingRefreshControl:));
}

- (id<RSBRefreshControlProtocol>)reverseInfiniteScrollingRefreshControl {
    return objc_getAssociatedObject(self, @selector(setReverseInfiniteScrollingRefreshControl:));
}

- (UIView *)contentSizeView {
    return objc_getAssociatedObject(self, @selector(setContentSizeView:));
}

- (NSLayoutConstraint *)contentSizeViewHeigthConstraint {
    return objc_getAssociatedObject(self, @selector(setContentSizeViewHeigthConstraint:));
}

- (BOOL)refreshControlShouldStop {
    return [objc_getAssociatedObject(self, @selector(setRefreshControlShouldStop:)) boolValue];
}

- (BOOL)infiniteControlShouldStop {
    return [objc_getAssociatedObject(self, @selector(setInfiniteControlShouldStop:)) boolValue];
}

- (BOOL)observing {
    return [objc_getAssociatedObject(self, @selector(setObserving:)) boolValue];
}

- (NSNumber *)initialBottomInsets {
    return objc_getAssociatedObject(self, @selector(setInitialBottomInsets:));
}

- (NSNumber *)initialTopInsets {
    return objc_getAssociatedObject(self, @selector(setInitialTopInsets:));
}

- (CGSize)lastContentSize {
    return [objc_getAssociatedObject(self, @selector(setLastContentSize:)) CGSizeValue];
}

- (BOOL)infiniteScrollControlHeightInstalled {
    return [objc_getAssociatedObject(self, @selector(setInfiniteScrollControlHeightInstalled:)) boolValue];
}

- (BOOL)reverseInfiniteScrollControlHeightInstalled {
    return [objc_getAssociatedObject(self, @selector(setReverseInfiniteScrollControlHeightInstalled:)) boolValue];
}

- (RSBPullToRefreshStyle)pullToRefreshStyle {
    return (RSBPullToRefreshStyle)[objc_getAssociatedObject(self, @selector(setPullToRefreshStyle:)) integerValue];
}

#pragma mark - RACSignals

#ifdef RSB_RC_WITH_REACTIVE

- (RACSignal *)rac_addInfiniteScrollingSignal {
    
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self addInfiniteScrollingWithActionHandler:^{
            [subscriber sendNext:nil];
        }];
        return nil;
    }];
}

- (RACSignal *)rac_addPullToRefreshSignal {
    
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self addPullToRefreshWithActionHandler:^{
            [subscriber sendNext:nil];
        }];
        return nil;
    }];
}

- (RACSignal *)rac_addReverseInfiniteScrollingSignal {
    
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self addReverseInfiniteScrollingWithActionHandler:^{
            [subscriber sendNext:nil];
        }];
        return nil;
    }];
}

- (RACSignal *)rac_addInfiniteScrollingWithRefreshControlSignal:(id<RSBRefreshControlProtocol>)refreshControl {
    
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self addInfiniteScrollingWithRefreshControl:refreshControl actionHandler:^{
            [subscriber sendNext:nil];
        }];
        return nil;
    }];
}

- (RACSignal *)rac_addPullToRefreshWithRefreshControlSignal:(id<RSBRefreshControlProtocol>)refreshControl {
    
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self addPullToRefreshWithRefreshControl:refreshControl actionHandler:^{
            [subscriber sendNext:nil];
        }];
        return nil;
    }];

}

#endif

@end
