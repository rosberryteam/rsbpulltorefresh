//
//  RSBViewController.m
//  RSBPullToRefresh
//
//  Created by Anton Kormakov on 05/04/2016.
//  Copyright (c) 2016 Anton Kormakov. All rights reserved.
//

#import "RSBViewController.h"
#import <RSBPullToRefresh/UIScrollView+RefreshControls.h>

@interface RSBViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *items;

@property (nonatomic, assign) NSUInteger appendIndex;
@property (nonatomic, assign) NSUInteger prependIndex;

@end

#define kLoadingDelay 1.0

@implementation RSBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self setupItems:20];
    
    __weak RSBViewController *weakSelf = self;
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kLoadingDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (weakSelf.appendIndex < 40) {
                [weakSelf appendItems:20];
            } else {
                [weakSelf.tableView.infiniteScrollingRefreshControl setEnabled:NO];
            }
            
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.infiniteScrollingRefreshControl endLoading];
        });
    }];
    
    /*
    [self.tableView addReverseInfiniteScrollingWithActionHandler:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kLoadingDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (weakSelf.prependIndex < 40) {
                [weakSelf prependItems:20];
            } else {
                [weakSelf.tableView.reverseInfiniteScrollingRefreshControl setEnabled:NO];
            }
            
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.reverseInfiniteScrollingRefreshControl endLoading];
        });
    }];
    */
    
    self.tableView.pullToRefreshStyle = RSBPullToRefreshStyleMovesWithContent;
    [self.tableView addPullToRefreshWithActionHandler:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kLoadingDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf setupItems:20];
            [weakSelf.tableView reloadData];
            
            [weakSelf.tableView.pullToRefreshControl endLoading];
            [weakSelf.tableView.infiniteScrollingRefreshControl setEnabled:YES];
        });
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView triggerPullToRefresh];
}

- (void)setupItems:(NSUInteger)count {
    self.appendIndex = 0;
    self.prependIndex = 0;
    
    self.items = [NSMutableArray array];
    [self appendItems:count];
}

- (void)appendItems:(NSUInteger)count {
    for (int i = 0; i < count; ++i) {
        [self.items addObject:@(self.appendIndex + i)];
    }
    self.appendIndex += count;
}

- (void)prependItems:(NSUInteger)count {
    for (int i = 0; i < count; ++i) {
        [self.items insertObject:@(-(int)(self.prependIndex + i + 1)) atIndex:0];
    }
    self.prependIndex += count;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", self.items[indexPath.row]];
    return cell;
}


@end
